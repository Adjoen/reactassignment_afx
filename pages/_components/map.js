import React, {
  forwardRef,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import GoogleMapReact from "google-map-react";
import { Image } from "../../components";
const MY_KEY = process.env.GOOGLE_API_KEY;

const Index = forwardRef(({}, ref) => {
  const defaultCenter = { lat: -6.2295712, lng: 106.7594781 };
  const defaultZoom = 11;

  const [mapConfig, setMapConfig] = useState({
    center: defaultCenter,
    zoom: defaultZoom,
  });

  const [currentLocation, setCurrentLocation] = useState({ lat: "", lng: "" });
  const [selectedLocation, setSelectedLocation] = useState({
    lat: "",
    lng: "",
  });

  const mapRef = useRef();
  const yourLocationRef = useRef();
  const selectedLocationRef = useRef();

  useImperativeHandle(
    ref,
    () => ({
      show(e) {
        const newCenter = {
          lat: e.geometry.location.lat(),
          lng: e.geometry.location.lng(),
        };
        updateMap({ center: newCenter, zoom: defaultZoom });
        selectedLocationRef.current.show(newCenter);
      },
      clear() {
        setSelectedLocation({ lat: "", lng: "" });
        selectedLocationRef.current.clear();
      },
    }),
    []
  );

  const handleApiLoaded = (map, maps) => {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(function (position) {
        const newCenter = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        updateMap({ center: newCenter, zoom: defaultZoom });
        yourLocationRef.current.show(newCenter);
      });
    } else {
      console.log("Not Available");
    }
  };

  const updateMap = (obj) =>
    setMapConfig((s) => ({
      ...s,
      ...obj,
    }));
  return (
    <GoogleMapReact
      bootstrapURLKeys={{ key: MY_KEY, language: "id", region: "id" }}
      defaultCenter={defaultCenter}
      defaultZoom={11}
      onChange={(e) => setMapConfig({ ...e })}
      {...mapConfig}
      ref={mapRef}
      yesIWantToUseGoogleMapApiInternals
      onGoogleApiLoaded={({ map, maps }) => handleApiLoaded(map, maps)}
    >
      <YourLocation
        ref={yourLocationRef}
        {...currentLocation}
        setLocation={setCurrentLocation}
      />

      <SelectedLocation
        ref={selectedLocationRef}
        {...selectedLocation}
        setLocation={setSelectedLocation}
      />
    </GoogleMapReact>
  );
});

export default Index;

const YourLocation = forwardRef(({ setLocation }, ref) => {
  const [visible, setVisible] = useState(false);

  useImperativeHandle(
    ref,
    () => ({
      show(e) {
        setVisible(true);
        setLocation(e);
      },
    }),
    []
  );
  return (
    visible && (
      <div className="flex items-center justify-center">
        <div className="absolute border-2  shadow shadow-gray-600 border-white rounded-full w-5 h-5">
          <div className="h-full w-full bg-blue-500 rounded-full" />
        </div>
      </div>
    )
  );
});

const SelectedLocation = forwardRef(({ setLocation }, ref) => {
  const [visible, setVisible] = useState(false);

  useImperativeHandle(
    ref,
    () => ({
      show(e) {
        setVisible(true);
        setLocation(e);
      },
      clear() {
        setVisible(false);
      },
    }),
    []
  );
  return (
    visible && (
      <div className="flex items-center justify-center">
        <div className="absolute h-7 w-7">
          <Image
            src="/pin-map.png"
            alt="me"
            className="shadow shadow-gray-600"
          />
        </div>
      </div>
    )
  );
});
