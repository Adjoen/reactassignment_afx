import ClearIcon from "@mui/icons-material/Clear";
import HistoryIcon from "@mui/icons-material/History";
import SearchIcon from "@mui/icons-material/Search";
import {
  IconButton,
  InputBase,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Paper,
} from "@mui/material";
import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import ReactDOM from "react-dom";
import { useDispatch, useSelector } from "react-redux";
import { addPlace, removePlace } from "../../redux/actions/places";
const Index = ({ onChange, clearMap }) => {
  const [value, setValue] = useState("");

  const searchBox = useRef();
  const inputRef = useRef();
  const historyRef = useRef();
  const dispatch = useDispatch();

  const onPlacesChanged = () => {
    const selected = searchBox.current.getPlaces()[0];
    dispatch(addPlace(selected));
    setValue(selected.formatted_address);
    onChange(selected);
  };

  useEffect(() => {
    var input = ReactDOM.findDOMNode(inputRef.current);
    searchBox.current = new google.maps.places.SearchBox(input);
    searchBox.current.addListener("places_changed", onPlacesChanged);

    return () => {
      google.maps.event.clearInstanceListeners(searchBox.current);
    };
  }, []);

  const clearValue = () => {
    setValue('');
    clearMap();
  };
  return (
    <div className="absolute top-0 left-0 z-10 m-3">
      <Paper component="form" sx={{ p: "2px 4px", width: 400 }}>
        <div className="flex items-center">
          <InputBase
            sx={{ ml: 1, flex: 1 }}
            placeholder="Cari di peta"
            value={value}
            onChange={(e) => setValue(e.target.value)}
            inputProps={{ "aria-label": "search google maps", ref: inputRef }}
          />
          <IconButton
            type="submit"
            sx={{ p: "10px" }}
            aria-label="search"
            disabled={value && undefined}
            onClick={() => clearValue()}
          >
            {!value ? <SearchIcon /> : <ClearIcon />}
          </IconButton>
          <IconButton
            type="submit"
            sx={{ p: "10px" }}
            aria-label="search"
            onClick={(e) => {
              e.preventDefault();
              historyRef.current.show();
            }}
          >
            <HistoryIcon />
          </IconButton>
        </div>
        <History onChange={onChange} ref={historyRef} />
      </Paper>
    </div>
  );
};

export default Index;

const History = forwardRef(({ onChange }, ref) => {
  const [show, setShow] = useState(false);
  const places = useSelector((s) => s.places);
  const dispatch = useDispatch();

  useImperativeHandle(
    ref,
    () => ({
      show() {
        setShow((s) => !s);
      },
    }),
    []
  );

  const deletePlace = (place_id) => {
    dispatch(removePlace({ place_id }));
    setShow(false);
  };
  const length = Object.keys(places).length;

  return (
    show && (
      <List className="overflow-auto" style={{ maxHeight: 400 }}>
        {length > 0 ? (
          Object.keys(places).map((x, i) => (
            <ListItem disablePadding key={i}>
              <ListItemButton
                component="a"
                href="#simple-list"
                onClick={() => {
                  onChange(places[x]);
                  setShow(false);
                }}
              >
                <ListItemIcon>
                  <HistoryIcon />
                </ListItemIcon>
                <ListItemText primary={places[x].formatted_address} />
              </ListItemButton>
              <IconButton
                type="submit"
                sx={{ p: "10px" }}
                aria-label="search"
                onClick={(e) => {
                  e.preventDefault();
                  deletePlace(x);
                }}
              >
                <ClearIcon />
              </IconButton>
            </ListItem>
          ))
        ) : (
          <div className="text-center">
            Belum ada tempat yang sudah pernah dicari
          </div>
        )}
      </List>
    )
  );
});
