import React from "react";
import Image from "next/image";

const Index = ({ src, alt = "undefined", className }) => {
  return (
    <div className={"image-container"}>
      <Image
        src={src}
        layout="fill"
        className={`image ${className}`}
        alt={alt}
      />
    </div>
  );
};

export default Index;
