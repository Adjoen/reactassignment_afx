import { PLACE_ADD, PLACE_REMOVE } from "../type";

const initialState = {};

// Sample json ../../sample_json/places.json
const obj_keys = ["formatted_address", "geometry"];

function placesReducers(state = initialState, { value, type }) {
  switch (type) {
    case PLACE_ADD:
      const isDuplicate = state[value.place_id] !== undefined;

      if (!isDuplicate) {
        const obj = {};
        for (const key of obj_keys) {
          obj[key] = value[key];
        }
        state[value.place_id] = obj;
      }

      return state;
    case PLACE_REMOVE:
      delete state[value.place_id];
      return state;
    default:
      return state;
  }
}
export default placesReducers;
