import {
  applyMiddleware,
  combineReducers,
  createStore,
} from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import placesReducers from "./reducers/places";

const rootReducer = combineReducers({
  places: placesReducers,
});
export const store = createStore(rootReducer, applyMiddleware(thunk));
